<?
	include "functions.php";
	
	$actions = array("add", "edit");
	
	if (in_array($_POST["action"], $actions))
	{
		$action = $_POST['action'];
		
		if ($action == "add")
		{
			$result = insert_report($_POST);
			if ($result)
				header("Location: reports.php");
		}
		
		if ($action == "edit")
		{
			$result = update_report($_POST);
			if ($result)
				header("Location: reports.php");
		}
		
		
	}
	
	
	
?>