<?
	session_start();
	
	if (!isset($_SESSION['user']) && !isset($_SESSION['org']))
		header("Location: index.php");
	
	$link = mysqli_connect("192.168.64.2", "user", "user", "innoreport");
	
	if (!$link) {
	    echo "Ошибка: Невозможно установить соединение с MySQL." . PHP_EOL;
	    echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
	    echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
	    exit;
	}
	
	include "classification.php";
	
	
	function getUser($login)
	{
		$query = "SELECT * FROM users WHERE login = '".$login."' LIMIT 1";
		global $link;
		$result = mysqli_query($link, $query);
		return $result;
	}
	

	function getReports($field, $id, $action)
	{
		if ($id > 0 || $action == "my")
			$WHERE = "WHERE ".$field." = '".$id."'";		
		
		if ($action == "all")
			$WHERE = "WHERE id_report > 0";
		
		if ($action == "new")
			$WHERE .= " AND status_report = 0";
			
		if ($action == "received")
			$WHERE .= " AND status_report = 1";
		
		if ($action == "in_progress")
			$WHERE .= " AND status_report = 2";
			
	
		global $link;
		$query = "SELECT *, 
		IF(r.id_org = 0, 'Not specified', o.name_org) as name_org,

		CASE
			WHEN status_report = 0 THEN 'NEW'
			WHEN status_report = 1 THEN 'RECEIVED'
			WHEN status_report = 2 THEN 'IN_PROGRESS'
			WHEN status_report = 3 THEN 'SOLVED'
			WHEN status_report = 4 THEN 'DECLINED'
			END AS status_report
		FROM reports r
		LEFT JOIN organizations o ON r.id_org = o.id_org ".$WHERE." ORDER BY status_report ASC, date_changed_report DESC";
		$result = mysqli_query($link, $query);
		return $result;
	}
	
	function getReportById($id)
	{
		global $link;
		$query = "SELECT * FROM reports 
		
		
		WHERE id_report = '".$id."' LIMIT 1 ";
		$result = mysqli_query($link, $query);
		return $row = mysqli_fetch_array($result);
	}	
	
	function getReportView($report, $action)
	{
		$str = '
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">
				<h1 class="uk-article-title uk-display-inline">'.$report["header_report"];
				
				
			if (isset($_SESSION["user"]) && $_SESSION["user"] == true && $report["status_report"] == "NEW" && $_SESSION['id_user'] == $report["id_user"]) // change if (user = true)
			$str .= '<a href = "report.php?action=edit&id='.$report['id_report'].'"><span uk-icon="icon: pencil; ratio: 1.5"></span></a>';	
				
			$str .= '</h1></div>
			<div class="uk-card-body">
				<p>'.$report["text_report"].'</p>
				<div>
					<div>
		            	<p class="uk-button uk-button-text" href="#">Keywords: '.$report["keywords"].'</a>
					</div>
					
					<div>
		            	<a class="uk-button uk-button-text" href="#">Organization: '.$report["name_org"].'</a>
					</div>
					<div>
		            	
					</div>
				</div>
				<p class="uk-article-meta">Date: '.date("d/m/Y H:i:s", strtotime($report["date_report"])).
					'<span class="uk-label uk-label-primary uk-margin-left">Status: '.$report["status_report"].'</span></p>';
				
		
		
			
			
		$status = array("NEW", "RECEIVED", "IN_PROGRESS", "SOLVED", "DECLINED");
			
		
		if (isset($_SESSION["org"]) && $_SESSION["org"] == true && $action != "all")
		{
			$str .= '
			<form action = "change_status.php" method = "POST" class = "uk-margin-top uk-width-large">
			<legend class="uk-legend">
				Change status:
			</legend>
			<div class="uk-margin uk-width-1-2">
	            <select class="uk-select" name = "status_report">';
	            
	                        
	        for($i = 1; $i < count($status); $i++)
	        {
		        if ($report["status_report"] == $status[$i])
		        	$str .= '<option value = "'.$i.'" selected>'.$status[$i].'</option>';
		        else
		        	$str .= '<option value = "'.$i.'">'.$status[$i].'</option>';
	        }
	                
	                
	                
	        $str .= '</select>
	        </div>
	        <input type = "hidden" value = "'.$report["id_report"].'" name = "id_report"/>
	        <button class = "uk-button uk-button-primary" type = "submit">Save</button>
			</form>';
		}
		
			
		
		$str .= '</div></div>';
		return $str;
	}
	
	function clean_keywords($keywords)
	{
		$keywords = trim($keywords);
		$keywords = strtolower($keywords);
		return $keywords_array = explode(",", $keywords);
		
	}
	
	function insert_report($report)
	{
		$keywords = clean_keywords($report["keywords"]);
		$result = classification($keywords);
		$row_id_org = mysqli_fetch_array($result);
		
		
		
		echo $query = "INSERT INTO reports(text_report, header_report, keywords, status_report, date_report, id_user, date_changed_report, id_org) 
			VALUES (
			'".$report["text_report"]."', 
			'".$report["header_report"]."', 
			'".$report["keywords"]."', 
			'0', 
			'".date("Y-m-d H:i:s")."', 
			'".$report["id_user"]."', 
			'".date("Y-m-d H:i:s")."', 
			'".$row_id_org['id_org']."')";
		
		global $link;
		$result = mysqli_query($link, $query);
		return $result;
	
	}
	
	function update_report($report)
	{
		$keywords = clean_keywords($report["keywords"]);
		$result = classification($keywords);
		$row_id_org = mysqli_fetch_array($result);
		
		$query = "UPDATE reports SET 
			text_report = '".$report["text_report"]."', 
			header_report = '".$report["header_report"]."', 
			keywords = '".$report["keywords"]."', 
			date_changed_report = '".date("Y-m-d H:i:s")."',
			id_org = '".$row_id_org['id_org']."'
			WHERE id_report = '".$report['id_report']."'";
		
		global $link;
		$result = mysqli_query($link, $query);
		return $result;
	
	}
	
	
	function update_report_status($id_report, $status_report)
	{
		$query = "UPDATE reports SET status_report = '".$status_report."', date_changed_report = '".date("Y-m-d H:i:s")."' WHERE id_report = '".$id_report."'";
		global $link;
		$result = mysqli_query($link, $query);
		return $result;
	}
	
	
?>