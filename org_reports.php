<?
	include "functions.php";
	$actions = array("my", "all", "new", "in_progress");
	$id_org = $_SESSION['id_org'];
					    
    if (isset($_GET["action"]))
    {
	    if (in_array($_GET["action"], $actions))
	    {
		    $action = $_GET["action"];
	    }
	}
	else
	{
		$action = "my";
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>InnoReport</title>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="assets/css/uikit.min.css" />
		<link rel="stylesheet" href="assets/css/ionicons.min.css">
		<link rel="stylesheet" href="assets/css/style.css" />
        <link rel="stylesheet" href="assets/css/notyf.min.css" />
		<script src="assets/js/uikit.min.js" ></script>
		<script src="assets/js/uikit-icons.min.js" ></script>
    </head>
	
	<body>
		<div uk-sticky class="uk-navbar-container tm-navbar-container uk-active">
            <div class="uk-container uk-container-expand">
                <nav uk-navbar>
                    <div class="uk-navbar-left">
                        <a href="#" class="uk-navbar-item uk-logo">
                            InnoReport
                        </a>
                    </div>
                    <div class="uk-navbar-right uk-light">
                        <ul class="uk-navbar-nav">
                            <li class="uk-active">
                                <a href="#">Actions<span class="ion-ios-arrow-down"></span></a>
                                <div uk-dropdown="pos: bottom-right; mode: click; offset: -17;">
                                   <ul class="uk-nav uk-navbar-dropdown-nav">
                                       <li class="uk-nav-header">Options</li>
                                       <li><a href="logout.php">Logout</a></li>
                                   </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        
        <div id="sidebar" class="tm-sidebar-left uk-background-default">
            <ul class="uk-nav uk-nav-default">
                <li class="uk-nav-header">
                    My reports
                </li>
                <li><a href="org_reports.php?action=my">View All</a></li>
                <li><a href="org_reports.php?action=new">View New</a></li>
                <li><a href="org_reports.php?action=in_progress">View In Progress</a></li>
                <li class="uk-nav-header">
                    All Reports
                </li>
                <li><a href="org_reports.php?action=all">View All</a></li>
            </ul>
        </div>
        <div class="content-padder content-background">
            <div class="uk-section-small uk-section-default header">
                <div class="uk-container uk-container-large">
                    <h1>My Reports</h1>
                </div>
            </div>
            <div class="uk-section-small">
                <div class="uk-container uk-container-large">
                    <div uk-grid class="uk-child-width-1-1@s uk-child-width-1-1@l">
                        
                        
                        <?
						$result = getReports("r.id_org", $id_org, $action);
						while ($row = mysqli_fetch_array($result)):?>
							<div>
                            	<?=getReportView($row, $action);?>
							</div>
							
						<? endwhile; ?>

                        
                        
                        
                    </div>
                </div>
            </div>
        </div>

	</body>
</html>
