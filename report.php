<?
	include "functions.php";
	$id_user = $_SESSION['id_user'];
	
	$actions = array("add", "edit");
	if (!isset($_GET["action"]) || !in_array($_GET["action"], $actions))
		$action = "add";
	else
		$action = $_GET["action"];
		
		
	if ($action == "edit")
	{
		$id_report = $_GET['id'];
		$report =  getReportById($id_report);
		if ($report['id_user'] != $id_user)
			header("Location: reports.php");
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Report List Panel</title>

		<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link rel="stylesheet" href="assets/css/uikit.min.css" />
				<link rel="stylesheet" href="assets/css/ionicons.min.css">
					<link rel="stylesheet" href="assets/css/style.css" />
					<link rel="stylesheet" href="assets/css/notyf.min.css" />
					<script src="assets/js/uikit.min.js" ></script>
					<script src="assets/js/uikit-icons.min.js" ></script>
				</head>
				<body>
					<div uk-sticky class="uk-navbar-container tm-navbar-container uk-active">
						<div class="uk-container uk-container-expand">
							<nav uk-navbar>
								<div class="uk-navbar-left">
									<a href="#" class="uk-navbar-item uk-logo">
                            InnoReport
									</a>
								</div>
								<div class="uk-navbar-right uk-light">
									<ul class="uk-navbar-nav">
										<li class="uk-active">
											<a href="#">Actions<span class="ion-ios-arrow-down"></span></a>
											<div uk-dropdown="pos: bottom-right; mode: click; offset: -17;">
												<ul class="uk-nav uk-navbar-dropdown-nav">
													<li class="uk-nav-header">Options</li>
													<li><a href="#">Logout</a></li>
												</ul>
											</div>
										</li>
									</ul>
								</div>
							</nav>
						</div>
					</div>
					<div id="sidebar" class="tm-sidebar-left uk-background-default">
			            <ul class="uk-nav uk-nav-default">
			
			                <li class="uk-nav-header">
								<a href="report.php?action=add">Create New</a>
							</li>
			                <li class="uk-nav-header">
			                    My reports
			                </li>
			                <li><a href="reports.php?action=my">View All</a></li>
			                <li><a href="reports.php?action=my">View In Progress</a></li>
			
			                <li class="uk-nav-header">
			                    All Reports
			                </li>
			                <li><a href="reports.php?action=all">View All</a></li>
			                <li><a href="reports.php?action=all">View In Progress</a></li>
			            </ul>
			        </div>
					<div class="content-padder content-background">
						<div class="uk-section-small uk-section-default header">
							<div class="uk-container uk-container-large">
								<h1>Reports</h1>
								<ul class="uk-breadcrumb">
									<li><a href="reports.php">Home</a></li>
									<li><span href="">Add or Edit</span></li>
								</ul>
							</div>
						</div>
						<div class="uk-section-small">
							<div class="uk-container uk-container-large">
								<div class="uk-card uk-card-default">
									<div class="uk-card-header">
										<? 
											if ($action == "add"): ?>
									        <legend class="uk-legend">Add new report</legend>
									        <? endif; ?>
									        <? if ($action == "edit"): ?>
									        <legend class="uk-legend">Edit report</legend>
									        <? endif; ?>
									</div>
									<div class="uk-card-body">	
										<form action = "save_report.php" method = "POST">
										    <fieldset class="uk-fieldset">
												
										
										        <div class="uk-margin">
										            <input class="uk-input" type="text" name = "header_report" placeholder="Report header" maxlength="60" 
										            	value = "<? if (isset($report["header_report"])) echo $report["header_report"]; ?>" required>
										        </div>
										
												<div class="uk-margin">
										            <textarea class="uk-textarea" rows="5" name = "text_report" placeholder="Report text" maxlength="500"><? if (isset($report["text_report"])) echo $report["text_report"]; ?></textarea>
										        </div>
					
												<div class="uk-margin">
										            <input class="uk-input" type="text" name = "keywords" placeholder="Keywords" maxlength="60" 
										            	value = "<? if (isset($report["keywords"])) echo $report["keywords"]; ?>" required>
										        </div>
												
												<input type = "hidden" name = "id_user" value = "<? echo $id_user; ?>" />
												
												<? if ($action == "add"): ?>
												<input type = "hidden" name = "action" value = "add" />
												<? endif; ?>
												
												<? if ($action == "edit"): ?>
												<input type = "hidden" name = "action" value = "edit" />
												<input type = "hidden" name = "id_report" value = "<? echo $id_report; ?>" />
												<? endif; ?>
												
												<button class="uk-button uk-button-primary">Save</button>
										    </fieldset>
										</form>
</div>
										</div>
									</div>
								</div>
							</div>
						</body>
					</html>
